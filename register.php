<?php

require_once "components.php";

if (Request::isPost()) {
    $query = "INSERT INTO user (login, password, is_admin) VALUES (:login, :password, :is_admin)";
    $data = array(
        ':login'    => Request::get('login'),
        ':password' => sha1(Request::get('password')),
        ':is_admin' => Request::get('is_admin'),
    );
    DatabaseConnection::getInstance()->insert($query, $data);
    header('Location: login.php');
}
?>

<form method="post">
    <input type="hidden" name="is_admin" value="0"/>
    <label for="login">Login:</label>
    <input type="text" name="login" id="login"/>

    <label for="password">Password:</label>
    <input type="password" name="password" id="password"/>

    <input type="submit" value="Sign In"/>
</form>